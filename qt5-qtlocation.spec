%global qt_module qtlocation

Summary:          Qt5 - Location component
Name:             qt5-%{qt_module}
Version:          5.15.10
Release:          2

License:          LGPL-3.0-only OR GPL-3.0-only WITH Qt-GPL-exception-1.0
Url:              http://www.qt.io
%global majmin %(echo %{version} | cut -d. -f1-2)
Source0:          https://download.qt.io/official_releases/qt/%{majmin}/%{version}/submodules/%{qt_module}-everywhere-opensource-src-%{version}.tar.xz

Patch0:           qtlocation-gcc10.patch
# https://invent.kde.org/qt/qt/qtlocation-mapboxgl/-/commit/35d566724c48180c9a372c2ed50a253871a51574
Patch1:           qtlocation-icu-75.patch

# filter plugin/qml provides
%global __provides_exclude_from ^(%{_qt5_archdatadir}/qml/.*\\.so|%{_qt5_plugindir}/.*\\.so)$

BuildRequires:    make
BuildRequires:    qt5-qtbase-devel >= 5.9.0
# QtPositioning core-private
BuildRequires:    qt5-qtbase-private-devel
%{?_qt5:Requires: %{_qt5}%{?_isa} = %{_qt5_version}}
BuildRequires:    qt5-qtdeclarative-devel >= 5.9.0

BuildRequires:    pkgconfig(zlib)
BuildRequires:    pkgconfig(icu-i18n)
BuildRequires:    pkgconfig(libssl)
BuildRequires:    pkgconfig(libcrypto)

%description
The Qt Location and Qt Positioning APIs gives developers the ability to
determine a position by using a variety of possible sources, including
satellite, or wifi, or text file, and so on.

%package devel
Summary:          Development files provided for qt5-qtlocation
Requires:         %{name} = %{version}-%{release} qt5-qtbase-devel
%description devel
%{summary}.

%package examples
Summary:          Programming examples for %{name}
Requires:         %{name}%{?_isa} = %{version}-%{release}
%description examples
%{summary}.


%prep
%autosetup -n qtlocation-everywhere-src-%{version} -p1

%build
%{qmake_qt5}

%make_build

%install
%make_install INSTALL_ROOT=%{buildroot}

## .prl/.la file love
# nuke .prl reference(s) to %%buildroot, excessive (.la-like) libs
pushd %{buildroot}%{_qt5_libdir}
for prl_file in libQt5*.prl ; do
  sed -i -e "/^QMAKE_PRL_BUILD_DIR/d" ${prl_file}
  if [ -f "$(basename ${prl_file} .prl).so" ]; then
    rm -fv "$(basename ${prl_file} .prl).la"
    sed -i -e "/^QMAKE_PRL_LIBS/d" ${prl_file}
  fi
done
popd


%ldconfig_scriptlets

%files
%license LICENSE.GPL* LICENSE.LGPL*
%{_qt5_libdir}/libQt5Location.so.5*
%{_qt5_archdatadir}/qml/QtLocation/
%{_qt5_libdir}/qt5/qml/Qt/labs/location/*
%{_qt5_plugindir}/geoservices/
%{_qt5_libdir}/libQt5Positioning.so.5*
%dir %{_qt5_archdatadir}/qml/QtPositioning
%{_qt5_archdatadir}/qml/QtPositioning/*
%{_qt5_plugindir}/position/
%{_qt5_libdir}/libQt5PositioningQuick.so.5*

%files devel
%{_qt5_headerdir}/QtLocation/
%{_qt5_libdir}/libQt5Location.so
%{_qt5_libdir}/libQt5Location.prl
%{_qt5_headerdir}/QtPositioning/
%{_qt5_libdir}/libQt5Positioning.so
%{_qt5_libdir}/libQt5Positioning.prl
%{_qt5_headerdir}/QtPositioningQuick/
%{_qt5_libdir}/libQt5PositioningQuick.so
%{_qt5_libdir}/libQt5PositioningQuick.prl
%{_qt5_libdir}/pkgconfig/Qt5Location.pc
%dir %{_qt5_libdir}/cmake/Qt5Location
%{_qt5_libdir}/cmake/Qt5Location/Qt5Location*.cmake
%{_qt5_archdatadir}/mkspecs/modules/qt_lib_location*.pri
%{_qt5_libdir}/pkgconfig/Qt5Positioning.pc
%dir %{_qt5_libdir}/cmake/Qt5Positioning
%{_qt5_libdir}/cmake/Qt5Positioning/Qt5Positioning*.cmake
%{_qt5_archdatadir}/mkspecs/modules/qt_lib_positioning*.pri
%{_qt5_libdir}/pkgconfig/Qt5PositioningQuick.pc
%dir %{_qt5_libdir}/cmake/Qt5PositioningQuick/
%{_qt5_libdir}/cmake/Qt5PositioningQuick/Qt5PositioningQuick*.cmake
%{_qt5_archdatadir}/mkspecs/modules/qt_lib_positioning*.pri

%files examples
%{_qt5_examplesdir}/


%changelog
* Fri Nov 01 2024 Funda Wang <fundawang@yeah.net> - 5.15.10-2
- fix build with icu 75

* Wed Aug 23 2023 peijiankang <peijiankang@kylinos.cn> - 5.15.10-1
- update to upstream version 5.15.10

* Wed Oct 13 2021 peijiankang <peijiankang@kylinos.cn> - 5.15.2-1
- update to upstream version 5.15.2

* Sat Jul 31 2021 wangyong<wangyong187@huawei.com> - 5.11.1-7
- Patch for GCC-10

* Mon Sep 14 2020 liuweibo <liuweibo10@huawei.com> - 5.11.1-6
- Fix Source0

* Fri Feb 14 2020 lingsheng <lingsheng@huawei.com> - 5.11.1-5
- Package init
